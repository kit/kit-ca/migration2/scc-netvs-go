package netvs

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/go-homedir"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/HereMobilityDevelopers/mediary"
)

const (
	DefaultAPIVersion = "3.2"
	URITemplate       = `https://%s/api/` + DefaultAPIVersion + `/wapi/transaction/execute?dry_mode=%s&dict_mode=%s`
	BASEURITEST       = "www-net-test.scc.kit.edu"
	BASEURIPROD       = "www-net.scc.kit.edu"
	BASEURIDEVEL      = "www-net-devel.scc.kit.edu"
)

// GetSessionToken returns a string containing a session token
type GetSessionToken func() string

type NETVSConfig struct {
	BaseURI      string
	APIVersion   string
	SessionToken GetSessionToken
}

type NETVSClient struct {
	NETVSConfig
	Client *http.Client
}

type TAResultArray []interface{}

func (r TAResultArray) NumResults() int {
	return len(r)
}

func (r TAResultArray) IsError() bool {
	return true
}

type TAResultDict map[string][]map[string]interface{}

func (r TAResultDict) NumResults() int {
	return len(r)
}

func (r TAResultDict) IsError() bool { // TODO
	return true
}

type AuthorizationError error

type InvalidRequest error

type InternalServerError error

type OtherUnexpectedResponseError error

type ClientConfig struct {
	ConfigDir            string
	ConfigFile           string
	ApiToken             string
	Endpoint             string
	Quiet                bool
	WaitForDNS           bool
	DNSWaitTimeout       time.Duration
	DNSTimeBetweenChecks time.Duration
}

// NewNETVSClientFromConfig creates a new NETVSClient from a NETVS config file and an endpoint.
// An empty endpoint creates a client that uses the default production endpoint.
func NewNETVSClientFromConfig(configfile string, endpoint string) (*NETVSClient, *NETVSCredentialINI, error) {
	// get path to config file; use default if empty
	if configfile == "" {
		configDir, err := homedir.Dir()
		if err != nil {
			return nil, nil, fmt.Errorf("unable to find homedir: %s", err)
		}

		configfile = filepath.Join(configDir, ".config", "netdb_client.ini")
	}

	// read and parse config file
	f, err := os.Open(configfile)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to open config file »%s«: %s", configfile, err)
	}

	Ini, err := ParseNETVSCredentialsINI(f)
	// file is readonly, errors on close can be ignored
	_ = f.Close()
	if err != nil {
		return nil, nil, fmt.Errorf("unable to parse config file: %s", err)
	}

	// build api client
	return NewNETVSClient(Ini.ToNETVSConfig(endpoint, DefaultAPIVersion)), Ini, nil
}

// NewNETVSClient creates a new NETVSClient that automatically adds authentication headers
func NewNETVSClient(config *NETVSConfig) *NETVSClient {
	client := mediary.Init().
		WithPreconfiguredClient(
			&http.Client{
				Transport: &http.Transport{
					Proxy: http.ProxyFromEnvironment,
					DialContext: (&net.Dialer{
						Timeout:   30 * time.Second,
						KeepAlive: 30 * time.Second,
					}).DialContext,
					ForceAttemptHTTP2:   true,
					IdleConnTimeout:     90 * time.Second,
					TLSHandshakeTimeout: 10 * time.Second,
					// this is currently required
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: false,
						Renegotiation:      tls.RenegotiateOnceAsClient,
					},
				},
				CheckRedirect: nil,
				Timeout:       60 * time.Second,
			}).
		AddInterceptors(func(req *http.Request, handler mediary.Handler) (*http.Response, error) {
			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("Authorization", "Bearer "+config.SessionToken())
			return handler(req)
		}).
		Build()

	return &NETVSClient{
		NETVSConfig: *config,
		Client:      client,
	}
}

func ReaderToString(r io.Reader) string {
	var buf bytes.Buffer
	_, _ = buf.ReadFrom(r)
	return buf.String()
}

type ModeSwitch bool

func (b ModeSwitch) ToString() string {
	if b {
		return "true"
	} else {
		return "false"
	}
}

func (c *NETVSClient) ExecuteTransactionRaw(t Transaction, dryMode ModeSwitch, dictMode ModeSwitch) ([]byte, error) {
	// return empty result if no transactions are specified
	if t.NumEntries() == 0 {
		return nil, nil
	}

	// create HTTP request
	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf(URITemplate, c.NETVSConfig.BaseURI, dryMode.ToString(), dictMode.ToString()),
		t.AsRequestBody())
	if err != nil {
		return nil, err
	}

	// send request
	resp, err := c.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func() {
		// Drain and close the body to let the Transport reuse the connection
		// see https://github.com/google/go-github/pull/317 for details
		_, _ = io.Copy(io.Discard, resp.Body)
		_ = resp.Body.Close()
	}()

	// handle errors based on HTTP return code
	switch resp.StatusCode {
	case 200: // success
		break
	case 400: // bad request
		return nil, InvalidRequest(fmt.Errorf(ReaderToString(resp.Body)))
	case 401: // Unauthorized
		return nil, AuthorizationError(fmt.Errorf(ReaderToString(resp.Body)))
	case 500: // internal server error
		return nil, InternalServerError(fmt.Errorf(ReaderToString(resp.Body)))
	default: // everything else
		return nil, OtherUnexpectedResponseError(errors.New(resp.Status))
	}

	// copy response body to buffer
	r, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (c *NETVSClient) ExecuteTransactionArray(t Transaction, dryMode ModeSwitch) (TAResultArray, error) {
	var response TAResultArray

	r, err := c.ExecuteTransactionRaw(t, dryMode, false)

	if err == nil && r == nil {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	// convert response to array of array
	err = json.Unmarshal(r, &response)
	if err != nil {
		return response, fmt.Errorf("unable to decode result as JSON: %w", err)
	}
	return response, nil
}

func (c *NETVSClient) ExecuteTransactionDict(t Transaction, dryMode ModeSwitch) (TAResultDict, error) {
	var response TAResultDict

	r, err := c.ExecuteTransactionRaw(t, dryMode, true)

	if err == nil && r == nil {
		return nil, nil
	}

	// convert response to array of array
	err = json.Unmarshal(r, &response)
	if err != nil {
		return response, fmt.Errorf("unable to decode result as JSON: %w", err)
	}
	return response, nil
}

type Transaction interface {
	// AsRequestBody returns the complete transaction as a string for the request body
	AsRequestBody() io.Reader
	// AddEntries appends transaction entries
	AddEntries(request ...TransactionEntry)
	// NumEntries returns the number of entries
	NumEntries() int
}

type TransactionEntry interface {
	// AsPart returns a single request as json object for the transaction body
	AsPart() string
}

// TransactionList holds the list of statements in a transaction.
// It implements the Transaction interface.
type TransactionList []TransactionEntry

// NewTARequestBody creates an empty transactionList
func NewTARequestBody() *TransactionList {
	return &TransactionList{}
}

func (body *TransactionList) AsRequestBody() io.Reader {
	var bodyreader = bytes.Buffer{}

	jsonbody, err := json.Marshal(body)
	if err != nil {
		return &bodyreader
	}
	bodyreader.Write(jsonbody)

	return &bodyreader
}

func (body *TransactionList) AddEntries(request ...TransactionEntry) {
	*body = append(*body, request...)
}

func (body *TransactionList) NumEntries() int {
	return len(*body)
}

// TAStatement represents a single transaction statement
type TAStatement struct {
	Name string             `json:"name"`
	Idx  string             `json:"idx,omitempty"`
	Old  TAStatementDataMap `json:"old,omitempty"`
	New  TAStatementDataMap `json:"new,omitempty"`
	Join map[string]string  `json:"join,omitempty"`
}

func (taStatement TAStatement) AsPart() string {
	jsonobj, err := json.Marshal(taStatement)
	if err != nil {
		return `{ name: "invalid.transaction" }`
	}
	return string(jsonobj)
}

// TAStatementDataMap is a typed map for members Old and New of TAStatement
type TAStatementDataMap map[string]interface{}
