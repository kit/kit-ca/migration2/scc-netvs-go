package netvs

import (
	"github.com/stretchr/testify/assert"
	"sort"
	"testing"
)

func TestGetUserFQDNs(t *testing.T) {
	var (
		Assert = assert.New(t)
		tests  = []struct {
			Username      string
			Domains       []string
			ExpectedError error
		}{
			{
				Username: "ge3242",
				Domains: []string{
					"ca.kit.edu.",
					"cert.kit.edu.",
					"does-not-exist.kit.edu.",
					"le.uni-beispiel.de.",
					"lehrer.partner.kit.edu.",
					"lists.kit.edu.",
					"netvs-unittests.scc.kit.edu.",
					"openpgpkey.kit.edu.",
					"scc.kit.edu.",
				},
				ExpectedError: nil,
			},
			{
				Username:      "no-such-user",
				Domains:       nil,
				ExpectedError: nil,
			},
		}
	)

	for _, test := range tests {
		domains, _, err := clientProduction.GetUserFQDNs(test.Username)
		sort.Strings(domains)
		Assert.EqualValues(test.Domains, domains)
		Assert.Equal(test.ExpectedError, err)
	}
}
