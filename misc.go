package netvs

import (
	"log"
	"os"
	"strings"
)

func GetTestTokenEnv(envvar string) func() string {
	return func() string {
		token := os.Getenv(envvar)
		if token == "" {
			log.Panicf("Could not load token. Environment variable '%s' is empty.", envvar)
		}
		return token
	}
}

func GetTestTokenMust(filename string) func() string {
	return func() string {
		tokenbytes, err := os.ReadFile(filename)
		if err != nil {
			panic(err)
		}
		return strings.TrimSpace(string(tokenbytes))
	}
}
