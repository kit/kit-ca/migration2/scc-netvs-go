package netvs

type DnsFqdn struct {
	Description          string   `mapstructure:"description"`
	Gpk                  string   `mapstructure:"gpk"`
	HasRr                bool     `mapstructure:"has_rr"`
	IsDhcp               bool     `mapstructure:"is_dhcp"`
	IsEmptyNonterminal   bool     `mapstructure:"is_empty_nonterminal"`
	IsHost               bool     `mapstructure:"is_host"`
	IsNonterminal        bool     `mapstructure:"is_nonterminal"`
	IsOwn                bool     `mapstructure:"is_own"`
	IsWildcard           bool     `mapstructure:"is_wildcard"`
	Label                string   `mapstructure:"label"`
	ParentValue          string   `mapstructure:"parent_value"`
	RadType              uint     `mapstructure:"rad_type"`
	RrChainTargetBcdList []string `mapstructure:"rr_chain_target_bcd_list"`
	RrChainTargetIsOwn   bool     `mapstructure:"rr_chain_target_is_own"`
	SubFqdnCount         uint     `mapstructure:"sub_fqdn_count"`
	Type                 string   `mapstructure:"type"`
	Value                string   `mapstructure:"value"`
	ValueIDNA            string   `mapstructure:"value_idna"`
	Zone                 string   `mapstructure:"zone"`
}

type DnsRecord struct {
	Data                  string   `mapstructure:"data"`
	Fqdn                  string   `mapstructure:"fqdn"`
	FqdnDescription       string   `mapstructure:"fqdn_description"`
	FqdnType              string   `mapstructure:"fqdn_type"`
	Gpk                   string   `mapstructure:"gpk"`
	HostIsNws             bool     `mapstructure:"host_is_nws"`
	IsOwn                 bool     `mapstructure:"is_own"`
	TargetBcdList         []string `mapstructure:"target_bcd_list"`
	TargetDataUnref       string   `mapstructure:"target_data_unref"`
	TargetFqdn            string   `mapstructure:"target_fqdn"`
	TargetFqdnType        string   `mapstructure:"target_fqdn_type"`
	TargetIpaddr          string   `mapstructure:"target_ipaddr"`
	TargetIsReverseUnique bool     `mapstructure:"target_is_reverse_unique"`
	TargetIsSingleton     bool     `mapstructure:"target_is_singleton"`
	Ttl                   int      `mapstructure:"ttl"`
	TtlResetDate          string   `mapstructure:"ttl_reset_date"`
	TtlResetDays          string   `mapstructure:"ttl_reset_days"`
	TtlZoneDefault        int      `mapstructure:"ttl_zone_default"`
	Type                  string   `mapstructure:"type"`
	Zone                  string   `mapstructure:"zone"`
}
