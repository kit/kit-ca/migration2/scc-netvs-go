package netvs

import (
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/miekg/dns"
	"github.com/stretchr/testify/assert"
)

var (
	fqdnDomain       string
	clientProduction *NETVSClient
)

func init() {
	clientProduction = NewNETVSClient(&NETVSConfig{
		BaseURI:      BASEURIPROD,
		APIVersion:   "3.2",
		SessionToken: GetTestTokenEnv("NETDB_API_TOKEN_PROD"),
	})
}

// EnvVariable represents an operating system environment variable
// that is either unset or has a string value (including the empty string)
type EnvVariable struct {
	name  string
	isSet bool
	value string
}

func (e EnvVariable) Name() string { return e.name }

// SetInOS sets the corresponding environment variable
func (e EnvVariable) SetInOS() error {
	if e.isSet {
		return os.Setenv(e.name, e.value)
	} else {
		return os.Unsetenv(e.name)
	}
}

// CreateUnsetEnvVariable create an EnvVariable that is unset and has no value
func CreateUnsetEnvVariable(name string) EnvVariable {
	return EnvVariable{
		name:  name,
		isSet: false,
		value: "",
	}
}

// CreateEnvVariable creates a set EnvVariable with a value
func CreateEnvVariable(name string, value string) EnvVariable {
	return EnvVariable{
		name:  name,
		isSet: true,
		value: value,
	}
}

// EnvVariableFromEnv retrieves an envvar from the OS and creates a EnvVariable for it
func EnvVariableFromEnv(name string) EnvVariable {
	val, isSet := os.LookupEnv(name)
	if isSet {
		return CreateEnvVariable(name, val)
	} else {
		return CreateUnsetEnvVariable(name)
	}
}

// EnvVariablesToOS sets (and unsets) the given array of EnvVariable in the OS
func EnvVariablesToOS(vars []EnvVariable) {
	for _, e := range vars {
		_ = e.SetInOS()
	}
}

// ReversibleEnvVariablesToOS sets (and unsets) the given array of EnvVariable in the OS
// and returns a function that reverts these settings.
func ReversibleEnvVariablesToOS(vars []EnvVariable) func() {
	var (
		originalEnvs []EnvVariable
	)
	// save original environment and set new one
	for _, e := range vars {
		originalEnvs = append(originalEnvs, EnvVariableFromEnv(e.name))
		_ = e.SetInOS()
	}
	return func() {
		EnvVariablesToOS(originalEnvs)
	}
}

func TestNewNETVSClientFromConfig(t *testing.T) {
	var (
		err error
	)

	// Tests for not existing default config file
	// Set homevar to ignore potentially existing personal netdb_client.ini
	env := []EnvVariable{
		CreateEnvVariable("HOME", "/tmp/this-shouldnt-exist"),
	}
	restoreFunc := ReversibleEnvVariablesToOS(env)
	_, _, err = NewNETVSClientFromConfig("", "irrelevant")
	assert.Error(t, err)
	restoreFunc()

	// Tests for not existing config file
	_, _, err = NewNETVSClientFromConfig("testdata/notExistingConfig.ini", "notexisting")
	assert.EqualError(t, err, "unable to open config file »testdata/notExistingConfig.ini«: open testdata/notExistingConfig.ini: no such file or directory")

	// Tests for bad config file (not parseable)
	_, _, err = NewNETVSClientFromConfig("testdata/config_noIniFormat.ini", "badsection")
	assert.Error(t, err)

	// Tests for valid config file
	_, ini, err := NewNETVSClientFromConfig("testdata/config_newStyle.ini", "existing")
	assert.NoError(t, err)
	expectedSection := NETVSCredentialSection{
		BaseURL: "api.uni-beispiel.de",
		Token:   "test-token",
	}
	assert.Equal(t, expectedSection, ini.Sections[ini.Endpoint])
}

func setupNetvsClientTest() {
	var err error

	ta := NewTARequestBody()

	fqdnDomain = "domain-" + sessionID.String() + "." + TestDomain
	Comment := fmt.Sprintf("Unittest für https://git.scc.kit.edu/KIT-CA/acme4netvs vom %s", time.Now().Format(time.RFC3339))

	ta.AddEntries(
		TAStatement{
			Name: "dns.fqdn.create",
			New: TAStatementDataMap{
				"type":        "domain",
				"value":       fqdnDomain,
				"description": Comment,
			},
		},
		TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn":                     fqdnDomain,
				"type":                     "A",
				"data":                     "172.21.73.58",
				"target_is_singleton":      false,
				"target_is_reverse_unique": false,
			},
		},
		TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn":                     fqdnDomain,
				"type":                     "AAAA",
				"data":                     "2a00:1398:4:14::2000:2000",
				"target_is_singleton":      false,
				"target_is_reverse_unique": false,
			},
		},
	)

	_, err = clientProduction.ExecuteTransactionArray(ta, false)
	if err != nil {
		log.Panicln(err)
	}
}

func cleanupNetvsClientTest() {
	var err error

	ta := NewTARequestBody()
	ta.AddEntries(
		TAStatement{
			Name: "dns.record.delete",
			Old: TAStatementDataMap{
				"fqdn": fqdnDomain,
				"type": "A",
				"data": "172.21.73.58",
			},
		},
		TAStatement{
			Name: "dns.record.delete",
			Old: TAStatementDataMap{
				"fqdn": fqdnDomain,
				"type": "AAAA",
				"data": "2a00:1398:4:14::2000:2000",
			},
		},
		// 2021-08-11: this is currently automated, but it won't be in a future API version
		/*		TAStatement{
				Name: "dns.fqdn.delete",
				Old: TAStatementDataMap{
					"value": fqdnDomain,
				},
			},*/
	)

	_, err = clientTest.ExecuteTransactionArray(ta, false)
	_ = err
	//if err != nil {
	//	log.Panicln(err)
	//}
}

func TestNewClient(t *testing.T) {
	var (
		config = NETVSConfig{
			BaseURI:      BASEURITEST,
			SessionToken: GetTestTokenEnv("NETDB_API_TOKEN_TEST"),
			APIVersion:   DefaultAPIVersion,
		}
		// single transaction requests as building blocks
		TAStatementDnsFqdnListCERT = TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": "cert.kit.edu",
			},
		}
		TAStatementDnsFqdnListDoesNotExists = TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": "does.not.exist",
			},
		}
		TAStatementDnsRecordCreateInvalid = TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn": "test.example.com",
				"type": "AAAA",
				"data": "2001:db8::feed:dead:beef",
			},
		}

		// array of all test transactions including expected result
		TestTA = []struct {
			TAList         []TransactionEntry
			ExpectError    bool
			ExpectedResult TAResultArray
		}{
			{
				TAList: []TransactionEntry{
					TAStatementDnsFqdnListCERT,
					TAStatementDnsFqdnListDoesNotExists,
				},
				ExpectError: false,
				ExpectedResult: TAResultArray{
					[]interface{}{},
					[]interface{}{},
				},
			},
			{
				TAList: []TransactionEntry{
					TAStatementDnsRecordCreateInvalid,
				},
				ExpectError: true,
			},
		}
	)

	// create NETVS client
	var client = NewNETVSClient(&config)

	for _, talist := range TestTA {
		// create ta
		ta := NewTARequestBody()

		// add transactions
		ta.AddEntries(talist.TAList...)

		// execute ta
		result, err := client.ExecuteTransactionArray(ta, false)

		if talist.ExpectError {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
			// 2022-08-17: return value to complex and volatile…
			_ = result
			//assert.Equal(t, result, talist.ExpectedResult)
		}
	}
}

/*
func TestDomainTypeDomain(t *testing.T) {
	ta := NewTARequestBody()

	hostname := fqdnHost

	ta.AddEntries(TAStatement{
		Name: "dns.fqdn.list",
		Old: TAStatementDataMap{
			"value": hostname,
		},
	})

	result, err := clientTest.ExecuteTransactionArray(ta, false, false)
	if err != nil {
		t.Error(err)
	}
	//_, _ = pp.Print(err, result)

	if result.NumResults() != 1 {
		t.Errorf("Incorrect number of results: %#v", result)
	}

	foo, ok := result[0].([]interface{})
	if !ok {
		t.Error(ok)
	}
	bar, ok := foo[0].(map[string]interface{})
	if !ok {
		t.Error(ok)
	}

	if bar["is_nonterminal"] == false {
		switch bar["type"] {
		case "host":
			changeTypeTA := NewTARequestBody()
			changeTypeTA.AddEntries(TAStatement{
				Name: "dns.fqdn.update",
				Old: TAStatementDataMap{
					"value": hostname,
				},
				New: TAStatementDataMap{
					"type": "domain",
				},
			})

			_, err = clientTest.ExecuteTransactionArray(changeTypeTA, false, false)
			if err != nil {
				t.Error(err)
			}

		default:
			t.Error("whut?")
		}
	} else {
		if !(bar["type"] == "domain" || bar["type"] == "alias") {
			t.Error("whut²?")
		}
	}
}
*/

func TestAllowedDomainsForUser(t *testing.T) {
	const username = "ge3242"
	var (
		domainLabels = make(map[string]int)
		checkDomains = []string{"test.cert.kit.edu", "nope.kit.edu", "extern.example.com", "foo.bar.blubb.scc.kit.edu", ""}
	)

	domains, _, err := clientProduction.GetUserFQDNs(username)
	if err != nil {
		t.Error(err)
	}

	// associate all domains with their number of labels
	for _, domain := range domains {
		domainLabels[domain] = dns.CountLabel(domain)
	}

	//
	var domainCheckResults = make(map[string]bool)
	for _, checkDomain := range checkDomains {
		checkDomainCanonical := dns.CanonicalName(checkDomain)
		for domain := range domainLabels {
			//fmt.Fprintf(os.Stderr, "%s (%d)\t%s -> %d\n", domain, labels, checkDomain, dns.CompareDomainName(domain, dns.CanonicalName(checkDomain)))
			if dns.IsSubDomain(domain, checkDomainCanonical) {
				domainCheckResults[checkDomainCanonical] = true
				_, _ = fmt.Fprintf(os.Stderr, "%s ✅\n", checkDomainCanonical)
			}
		}
	}
}
