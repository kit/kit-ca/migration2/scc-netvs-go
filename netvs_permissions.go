package netvs

import (
	"errors"
	"github.com/miekg/dns"
)

func (c *NETVSClient) GetUserFQDNs(username string) (domains []string, fullAnswer TAResultDict, err error) {
	userHasRightsTA := NewTARequestBody()
	userHasRightsTA.AddEntries(
		TAStatement{
			Name: "cntl.mgr.list",
			Idx:  "mgr",
			Old:  TAStatementDataMap{"login_name_list": []string{username}},
		},
		TAStatement{
			Name: "cntl.mgr2group.list",
			Idx:  "mgr2group",
			Join: map[string]string{"mgr": "default"},
		},
		TAStatement{
			Name: "cntl.mgr2ou.list",
			Idx:  "mgr2ou",
			Join: map[string]string{"mgr": "default"},
		},
		TAStatement{
			Name: "cntl.group.list",
			Idx:  "groups",
			Join: map[string]string{"mgr2group": "default"},
		},
		TAStatement{
			Name: "org.unit.list",
			Idx:  "ous",
			Join: map[string]string{"mgr2ou": "default"},
		},
		TAStatement{
			Name: "dns.fqdn2group.list",
			Idx:  "group2fqdn",
			Join: map[string]string{"groups": "default"},
		},
		TAStatement{
			Name: "dns.fqdn2ou.list",
			Idx:  "ou2fqdn",
			Join: map[string]string{"ous": "default"},
		},
	)

	ret, err := c.ExecuteTransactionDict(userHasRightsTA, false)

	if err != nil {
		return domains, ret, err
	}

	var (
		exists    bool
		domainMap = make(map[string]bool)
	)

	// collect all domains in canonical form
	_, exists = ret["group2fqdn"]
	if !exists {
		return domains, ret, errors.New("dict key group2fqdn does not exist")
	}
	for _, domain := range ret["group2fqdn"] {
		domain, exists := domain["fqdn_value"]
		if exists {
			domainMap[dns.CanonicalName(domain.(string))] = true
		}
	}
	_, exists = ret["ou2fqdn"]
	if !exists {
		return domains, ret, errors.New("dict key ou2fqdn does not exist")
	}
	for _, domain := range ret["ou2fqdn"] {
		domain, exists := domain["fqdn_value"]
		if exists {
			domainMap[dns.CanonicalName(domain.(string))] = true
		}
	}

	// collate domains
	for domain, _ := range domainMap {
		domains = append(domains, domain)
	}

	return domains, ret, nil
}
