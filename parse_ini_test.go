package netvs

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type NetvsIniTests []struct {
	INIFile          string
	ExpectParseErr   bool
	ExpectedEndpoint string
	ExpectedSection  NETVSCredentialSection
	WantEqual        bool
}

func TestParseNETVSCredentialsINI(t *testing.T) {
	tests := NetvsIniTests{
		{
			INIFile:          "testdata/config_newStyle.ini",
			ExpectParseErr:   false,
			ExpectedEndpoint: "existing",
			ExpectedSection: NETVSCredentialSection{
				BaseURL: "api.uni-beispiel.de",
				Token:   "test-token",
			},
		},
		{
			INIFile:          "testdata/config_oldStyle.ini",
			ExpectParseErr:   false,
			ExpectedEndpoint: "api.uni-beispiel.de",
			ExpectedSection: NETVSCredentialSection{
				BaseURL: "api.uni-beispiel.de",
				Token:   "test-token",
			},
		},
		{
			INIFile:        "testdata/config_noIniFormat.ini",
			ExpectParseErr: true,
		},
		{
			INIFile:        "testdata/notexisting.ini",
			ExpectParseErr: true,
		},
		{
			INIFile:        "testdata/config_noEndpoint.ini",
			ExpectParseErr: true,
		},
		{
			INIFile:        "testdata/config_noToken.ini",
			ExpectParseErr: true,
		},
		{
			INIFile:        "testdata/config_missingSection.ini",
			ExpectParseErr: true,
		},
	}

	for _, test := range tests {
		file, _ := os.Open(test.INIFile)
		TestConfig, err := ParseNETVSCredentialsINI(file)
		if test.ExpectParseErr {
			assert.Error(t, err)
		} else {
			// Check if enpoint is read correctly
			assert.Equal(t, test.ExpectedEndpoint, TestConfig.Endpoint)

			// check section contents for default endpoint
			assert.Equal(t, test.ExpectedSection, TestConfig.Sections[TestConfig.Endpoint])
		}
	}
}

type ToNETVSConfigTests []struct {
	endpoint        string
	config          *NETVSCredentialINI
	expectedBaseUIR string
	expectedToken   string
}

func TestNETVSCredentialINI_ToNETVSConfig(t *testing.T) {
	file, _ := os.Open("testdata/config_newStyle.ini")
	TestConfig, err := ParseNETVSCredentialsINI(file)
	assert.NoError(t, err)

	tests := ToNETVSConfigTests{
		{
			endpoint:        "",
			config:          TestConfig,
			expectedBaseUIR: "api.uni-beispiel.de",
			expectedToken:   "test-token",
		},
		{
			endpoint:        "existing",
			config:          TestConfig,
			expectedBaseUIR: "api.uni-beispiel.de",
			expectedToken:   "test-token",
		},
	}

	for _, test := range tests {
		result := test.config.ToNETVSConfig(test.endpoint, DefaultAPIVersion)
		assert.Equal(t, test.expectedBaseUIR, result.BaseURI)
	}
}
