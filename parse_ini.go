package netvs

import (
	"errors"
	"fmt"
	"io"

	"gopkg.in/ini.v1"
)

type NETVSCredentialSection struct {
	BaseURL string
	Token   string
}

type NETVSCredentialINI struct {
	Endpoint string
	Sections map[string]NETVSCredentialSection
}

func (c *NETVSCredentialINI) GetSections() []string {
	var sections []string
	for key := range c.Sections {
		sections = append(sections, key)
	}
	return sections
}

func (c *NETVSCredentialINI) ToNETVSConfig(endpoint, apiVersion string) *NETVSConfig {
	var baseURL string

	// default to DEFAULT endpoint if none is requested
	if endpoint == "" {
		endpoint = c.Endpoint
	}

	// default to production endpoint
	if section, exists := c.Sections[endpoint]; exists {
		baseURL = section.BaseURL
	} else {
		baseURL = BASEURIPROD
	}

	return &NETVSConfig{
		BaseURI: baseURL,
		SessionToken: func() string {
			return c.Sections[endpoint].Token
		},
		APIVersion: apiVersion,
	}
}

func ParseNETVSCredentialsINI(inicontents io.Reader) (*NETVSCredentialINI, error) {
	var (
		credentials = new(NETVSCredentialINI)
		err         error
	)

	credentials.Sections = make(map[string]NETVSCredentialSection)

	inibytes, err := io.ReadAll(inicontents)
	if err != nil {
		return nil, err
	}

	IniFile, err := ini.Load(inibytes)
	if err != nil {
		return nil, err
	}

	DefaultEndpoint := IniFile.Section("DEFAULT").Key("endpoint").String()
	if DefaultEndpoint == "" {
		return nil, errors.New("INI file does not contain an endpoint in the DEFAULT section")
	}
	credentials.Endpoint = DefaultEndpoint
	IniFile.DeleteSection("DEFAULT")

	sectionNames := make(map[string]bool)
	for _, section := range IniFile.Sections() {
		name := section.Name()
		sectionNames[name] = true
		section := NETVSCredentialSection{
			BaseURL: section.Key("base_url").String(),
			Token:   section.Key("token").String(),
		}
		switch {
		case section.Token == "":
			return nil, errors.New(fmt.Sprintf("Section »%s« has no token value", name))
		case section.BaseURL == "":
			section.BaseURL = name
		}
		credentials.Sections[name] = section
	}

	_, exists := sectionNames[DefaultEndpoint]
	if !exists {
		return nil, errors.New(fmt.Sprintf("INI file specifies default endpoint »%s« but there is no such section", DefaultEndpoint))
	}

	return credentials, nil
}
