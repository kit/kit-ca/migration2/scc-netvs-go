package netvs

import (
	"log"
	"os"
	"testing"

	"github.com/google/uuid"
)

const (
	TestDomain = "netvs-unittests.scc.kit.edu"
)

var (
	sessionID  uuid.UUID
	clientTest *NETVSClient
)

func init() {
	var err error

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	clientTest = NewNETVSClient(&NETVSConfig{
		BaseURI:      BASEURITEST,
		SessionToken: GetTestTokenEnv("NETDB_API_TOKEN_TEST"),
		APIVersion:   DefaultAPIVersion,
	})

	sessionID, err = uuid.NewRandom()
	if err != nil {
		panic(err)
	}

}

func TestMain(m *testing.M) {
	// prepare test environment
	setupNetvsClientTest()

	// run tests
	ret := m.Run()

	// cleanup test environment
	cleanupNetvsClientTest()

	os.Exit(ret)
}
